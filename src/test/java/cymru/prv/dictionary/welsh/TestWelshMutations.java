package cymru.prv.dictionary.welsh;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.LinkedList;


public class TestWelshMutations {

    private void test(WelshMutation mutation, String radical, String soft, String nasal, String aspirate){
        Assertions.assertEquals(radical, mutation.getRadical());
        Assertions.assertEquals(soft, mutation.getSoft());
        Assertions.assertEquals(nasal, mutation.getNasal());
        Assertions.assertEquals(aspirate, mutation.getAspirate());

        var expectedVersions = new LinkedList<>();
        expectedVersions.add(radical);
        if(soft != null)
            expectedVersions.add(soft);
        if(nasal != null)
            expectedVersions.add(nasal);
        if(aspirate != null)
            expectedVersions.add(aspirate);

        Assertions.assertIterableEquals(expectedVersions, mutation.getVersions());

        var obj = mutation.toJson();
        Assertions.assertEquals(radical, obj.get("init"));
        Assertions.assertEquals(soft != null ? soft : "", obj.get("soft"));
        Assertions.assertEquals(nasal != null ? nasal : "", obj.get("nasal"));
        Assertions.assertEquals(aspirate != null ? aspirate : "", obj.get("aspirate"));
    }

    @Test
    public void testDoesntThrowIfStringIsEmpty(){
        Assertions.assertDoesNotThrow(() -> new WelshMutation(""));
    }

    @Test
    public void testWordsStartingWithB(){
        WelshMutation mut = new WelshMutation("bangor");
        test(mut, "bangor", "fangor", "mangor", null);
    }

    @Test
    public void testWordsStartingWithC(){
        WelshMutation mut = new WelshMutation("caru");
        test(mut, "caru", "garu", "ngharu", "charu");
    }

    @Test
    public void testWordsStartingWithCh(){
        WelshMutation mut = new WelshMutation("chwarae");
        test(mut, "chwarae", null, null, null);
    }

    @Test
    public void testWordsStartingWithD(){
        WelshMutation mut = new WelshMutation("daeth");
        test(mut, "daeth", "ddaeth", "naeth", null);
    }

    @Test
    public void testWordsStartingWithDd(){
        WelshMutation mut = new WelshMutation("ddoe");
        test(mut, "ddoe", null, null, null);
    }

    @Test
    public void testWordsStartingWithG(){
        WelshMutation mut = new WelshMutation("gwm");
        test(mut, "gwm", "wm", "ngwm", null);
    }

    @Test
    public void testWordsStartingWithM(){
        WelshMutation mut = new WelshMutation("maneg");
        test(mut, "maneg", "faneg", null, null);
    }

    @Test
    public void testWordsStartingWithLl(){
        WelshMutation mut = new WelshMutation("llaw");
        test(mut, "llaw", "law", null, null);
    }

    @Test
    public void testWordsStartingWithP(){
        WelshMutation mut = new WelshMutation("peth");
        test(mut, "peth", "beth", "mheth", "pheth");
    }

    @Test
    public void testWordsStartingWithPh(){
        WelshMutation mut = new WelshMutation("pherm");
        test(mut, "pherm", null, null, null);
    }

    @Test
    public void testWordsStartingWithRh(){
        WelshMutation mut = new WelshMutation("rhan");
        test(mut, "rhan", "ran", null, null);
    }

    @Test
    public void testWordsStartingWithT(){
        WelshMutation mut = new WelshMutation("tan");
        test(mut, "tan", "dan", "nhan", "than");
    }

    @Test
    public void testWordsStartingWithTh(){
        WelshMutation mut = new WelshMutation("theatr");
        test(mut, "theatr", null, null, null);
    }
}
