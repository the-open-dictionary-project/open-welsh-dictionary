package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.json.Json;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Map;

public class TestWelshAdjective {

    @Test
    public void testOnSyllableWord(){
        WelshAdjective adj = new WelshAdjective(
                new JSONObject()
                    .put("normalForm", "test")
        );
        var obj = adj.toJson();
        var infl = obj.getJSONObject("inflections");
        Assertions.assertIterableEquals(List.of("tested"), Json.getStringList(infl, "equative"));
        Assertions.assertIterableEquals(List.of("testach"), Json.getStringList(infl, "comparative"));
        Assertions.assertIterableEquals(List.of("testaf"), Json.getStringList(infl, "superlative"));
    }

    @Test
    public void testComparableKeyWord(){
        WelshAdjective adj = new WelshAdjective(new JSONObject()
                .put("normalForm", "test")
                .put("comparable", false)
        );
        var obj = adj.toJson();
        Assertions.assertFalse(obj.has("inflections"));
    }
}
