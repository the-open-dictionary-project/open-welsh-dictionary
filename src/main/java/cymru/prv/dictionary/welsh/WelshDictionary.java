package cymru.prv.dictionary.welsh;

import cymru.prv.dictionary.common.Dictionary;
import cymru.prv.dictionary.common.DictionaryList;
import cymru.prv.dictionary.common.Word;
import cymru.prv.dictionary.common.WordType;
import org.json.JSONObject;

import java.util.*;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.regex.Pattern;


/**
 * Represents a Welsh dictionary.
 *
 * Creates a dictionary with the code "cy"
 *
 * @author Preben Vangberg
 * @since 1.0.0
 * @see Dictionary
 */
public class WelshDictionary extends Dictionary {

    private static final String LANG_CODE = "cy";

    private static final Map<WordType, Function<JSONObject, Word>> types = Map.of(
            WordType.adjective, WelshAdjective::new,
            WordType.conjunction, (w) -> new Word(w, WordType.conjunction),
            WordType.noun, WelshNoun::new,
            WordType.adposition, WelshAdposition::new,
            WordType.verb, WelshVerb::new
    );

    public WelshDictionary() {
        super(LANG_CODE, types);
    }

    public WelshDictionary(DictionaryList list) {
        super(list, LANG_CODE, types);
    }

    @Override
    public String getVersion() {
        return "1.2.0";
    }

    /**
     * Fetches a list of potential lemmas for the given form.
     * Overrides the default behaviour to search for mutated forms
     * if no lemma is returned on the first call to super.
     *
     * @param form The form to fetch the lemmas for.
     * @return A list of potential lemmas for the word
     */
    @Override
    public List<Word> getLemmas(String form) {
        var lemmas = super.getLemmas(form);
        if(lemmas.size() > 0)
            return lemmas;

        var mutations = Map.of(
                "p", Pattern.compile("^(b|mh|ph)"),
                "t", Pattern.compile("^(d|nh|th)"),
                "c", Pattern.compile("^(g|ngh|ch)"),
                "b", Pattern.compile("^([fm])(?=[^h])"),
                "d", Pattern.compile("^(dd|n)"),
                "g", Pattern.compile("^([aâeêiîoôwyŷlr]|ng)"),
                "ll", Pattern.compile("^l(?=[^l])"),
                "rh", Pattern.compile("^r(?=[^h])"),
                "m", Pattern.compile("^f(?=[^f])")
        );

        Set<String> potentialForms = new HashSet<>();
        for(var x : mutations.entrySet()){
            var match = x.getValue().matcher(form).replaceFirst(x.getKey());
            if(!match.equals(form))
                potentialForms.add(match);
        }

        for(var potentialForm : potentialForms){
            lemmas.addAll(super.getLemmas(potentialForm));
        }

        return lemmas;
    }
}
