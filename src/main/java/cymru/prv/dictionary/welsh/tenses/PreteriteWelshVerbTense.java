package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.Collections;
import java.util.List;


/**
 * Represents the preterite tense in Welsh
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class PreteriteWelshVerbTense extends WelshVerbTense {

    public PreteriteWelshVerbTense(WelshVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.singletonList(apply("ais"));
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Collections.singletonList(apply("aist"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.singletonList(apply("odd"));
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.singletonList(apply("on"));
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Collections.singletonList(apply("och"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.singletonList(apply("on"));
    }

}
