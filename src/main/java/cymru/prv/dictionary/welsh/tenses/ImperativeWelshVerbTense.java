package cymru.prv.dictionary.welsh.tenses;

import cymru.prv.dictionary.welsh.WelshVerb;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;


/**
 * Represents the imperative tense in Welsh
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
public class ImperativeWelshVerbTense extends WelshVerbTense {

    public ImperativeWelshVerbTense(WelshVerb verb, JSONObject obj) {
        super(verb, obj);
    }

    @Override
    protected List<String> getDefaultSingularFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultSingularSecond() {
        return Arrays.asList(apply("a"));
    }

    @Override
    protected List<String> getDefaultSingularThird() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultPluralFirst() {
        return Collections.emptyList();
    }

    @Override
    protected List<String> getDefaultPluralSecond() {
        return Arrays.asList(apply("wch"));
    }

    @Override
    protected List<String> getDefaultPluralThird() {
        return Collections.emptyList();
    }

}
